import React from 'react';
import imagenotify from './notify.png';

class Notify extends React.Component{
    render(){
        return(
            <div className="NotifyPage">
                <img alt="Icon Nofify" className="notifyicon" src={imagenotify} />
                <h1>Sorry, you don't have any notification at this time.</h1>
                <p>We will notify you at first time for great info! </p>
            </div>
        )
    }
}

export default Notify