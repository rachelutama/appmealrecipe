import React from 'react';
import './style.css';
import Login from './login';
import Home from './home';
import Navigasi from './navigasi';
import RecipeGallery from './recipe.js';
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import Notify from './notify';

function App() {
  return (
    <BrowserRouter>
      <Switch>
        <Route path="/" exact component = {Login} />
        <Route path="/home" exact component = {Home} />
        <Route path="/recipe" exact component = {RecipeGallery} />
        <Route path="/notify" exact component = {Notify} />
      </Switch>
      <Navigasi />
    </BrowserRouter>

  );
}

export default App;
