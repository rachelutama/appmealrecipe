import React from 'react';
import {Link} from 'react-router-dom';

class Navigasi extends React.Component{
    render(){
        return(
            <div>
                <nav className="navbar">
                    <ul>
                        <li><Link to="/home" ><i className="fa fa-2x fa-home"></i>Home</Link></li>
                        <li><Link to="/notify"><i className="fa fa-2x fa-bell"></i>Updates</Link></li>
                        <li><Link to="/recipe" ><i className="fa fa-2x fa-utensils"></i>Recipe</Link></li>
                        <li><Link to="/" ><i className="fa fa-2x fa-user-circle"></i>User</Link></li>
                    </ul>
                    
                </nav>
            </div>
        )
    }
}

export default Navigasi